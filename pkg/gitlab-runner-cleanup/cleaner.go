package gitlabrunnercleanup

import (
	"errors"
	"fmt"
	"net/url"
	"path"
	"time"

	gitlab "gitlab.com/gitlab-org/api/client-go"
	"go.uber.org/zap"
)

const RunnersPerPage = 10

var (
	ErrNoLogger      = errors.New("no logger")
	ErrRequestFailed = errors.New("request failed")
)

type CleanerConfig struct {
	Log      *zap.SugaredLogger
	URL      string
	Token    string
	Projects []string
	Groups   []string
	Delay    time.Duration
}

type Cleaner struct {
	log      *zap.SugaredLogger
	client   *gitlab.Client
	projects []string
	groups   []string
	delay    time.Duration
}

func NewCleaner(config CleanerConfig) (*Cleaner, error) {
	if config.Log == nil {
		return nil, ErrNoLogger
	}

	// create API url
	url, err := url.Parse(config.URL)
	if err != nil {
		return nil, fmt.Errorf("failed parsing GitLab URL: %w", err)
	}

	url.Path = path.Join(url.Path, "/api/v4")

	// create GitLab client
	client, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(url.String()))
	if err != nil {
		return nil, fmt.Errorf("failed create GitLab client: %w", err)
	}

	cleaner := Cleaner{
		log:      config.Log,
		client:   client,
		projects: config.Projects,
		groups:   config.Groups,
		delay:    config.Delay,
	}

	return &cleaner, nil
}

func (c *Cleaner) Cleanup() (int, error) {
	count := 0

	runners, _, err := c.client.Runners.ListRunners(&gitlab.ListRunnersOptions{
		ListOptions: gitlab.ListOptions{PerPage: RunnersPerPage, Page: 0},
	})
	if err != nil {
		return count, fmt.Errorf("%w: %s", ErrRequestFailed, err.Error())
	}

	for _, runner := range runners {
		success, err := c.processRunner(runner)
		if err != nil {
			c.log.Errorw("failed to process runner",
				"id", runner.ID,
				"name", runner.Name,
				"description", runner.Description,
				"error", err.Error())

			continue
		}

		if success {
			count++
		}
	}

	return count, nil
}

func (c *Cleaner) processRunner(runner *gitlab.Runner) (bool, error) {
	details, _, err := c.client.Runners.GetRunnerDetails(runner.ID)
	if err != nil {
		return false, fmt.Errorf("%w: %s", ErrRequestFailed, err.Error())
	}

	if !c.runnerValidForCleanup(details) {
		c.log.Debugw("runner not valid for cleanup",
			"id", runner.ID,
			"name", runner.Name,
			"description", runner.Description)

		return false, nil
	}

	if !c.runnerInGroupOrProject(details) {
		c.log.Debugw("runner not in enabled project/group",
			"id", runner.ID,
			"name", runner.Name,
			"description", runner.Description)

		return false, nil
	}

	_, err = c.client.Runners.RemoveRunner(runner.ID)
	if err != nil {
		c.log.Errorw("failed to remove runner",
			"id", runner.ID,
			"name", runner.Name,
			"description", runner.Description,
			"error", err.Error())

		return false, nil
	}

	c.log.Infow("removed runner",
		"id", runner.ID,
		"name", runner.Name,
		"description", runner.Description)

	return true, nil
}

func (c *Cleaner) runnerValidForCleanup(runner *gitlab.RunnerDetails) bool {
	if runner.Status == "online" {
		return false
	}

	if runner.ContactedAt == nil || time.Since(*runner.ContactedAt) > c.delay {
		return true
	}

	return false
}

func (c *Cleaner) runnerInGroupOrProject(runner *gitlab.RunnerDetails) bool {
	if len(c.projects) == 0 && len(c.groups) == 0 {
		return true
	}

	for _, project := range runner.Projects {
		for _, name := range c.projects {
			if project.NameWithNamespace == name {
				return true
			}
		}
	}

	for _, group := range runner.Groups {
		for _, name := range c.groups {
			if group.Name == name {
				return true
			}
		}
	}

	return false
}
