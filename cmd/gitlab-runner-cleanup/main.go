package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	cleanup "gitlab.com/ydkn/gitlab-runner-cleanup/pkg/gitlab-runner-cleanup"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	apiURLDefault   = "https://gitlab.com"
	delayDefault    = 720 * time.Hour
	logLevelDefault = "info"
)

var errInvalidLogLevel = errors.New("invalid log level")

func main() {
	cobra.OnInitialize(func() {
		viper.AutomaticEnv()
	})

	//nolint:exhaustivestruct
	rootCmd := &cobra.Command{
		Use:  "gitlab-runner-cleanup",
		RunE: run,
	}

	rootCmd.PersistentFlags().String("url", apiURLDefault, "API URL")
	_ = viper.BindPFlag("url", rootCmd.PersistentFlags().Lookup("url"))
	viper.SetDefault("url", rootCmd.PersistentFlags().Lookup("url").DefValue)

	rootCmd.PersistentFlags().String("token", "", "API token")
	_ = viper.BindPFlag("token", rootCmd.PersistentFlags().Lookup("token"))
	viper.SetDefault("token", rootCmd.PersistentFlags().Lookup("token").DefValue)

	rootCmd.PersistentFlags().StringSlice("projects", []string{}, "projects to cleanup")
	_ = viper.BindPFlag("projects", rootCmd.PersistentFlags().Lookup("projects"))
	viper.SetDefault("projects", []string{})

	rootCmd.PersistentFlags().StringSlice("groups", []string{}, "groups to cleanup")
	_ = viper.BindPFlag("groups", rootCmd.PersistentFlags().Lookup("groups"))
	viper.SetDefault("groups", []string{})

	rootCmd.PersistentFlags().Duration("delay", delayDefault, "Delay since last contact")
	_ = viper.BindPFlag("delay", rootCmd.PersistentFlags().Lookup("delay"))
	viper.SetDefault("delay", rootCmd.PersistentFlags().Lookup("delay").DefValue)

	rootCmd.PersistentFlags().String("log-level", logLevelDefault, "log level")
	_ = viper.BindPFlag("log_level", rootCmd.PersistentFlags().Lookup("log-level"))
	viper.SetDefault("log_level", rootCmd.PersistentFlags().Lookup("log-level").DefValue)

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err) //nolint:forbidigo
		os.Exit(1)
	}
}

func run(_ *cobra.Command, _ []string) error {
	logger, err := newLogger(viper.GetString("log_level"))
	if err != nil {
		return fmt.Errorf("failed to create logger: %w", err)
	}

	defer func() { _ = logger.Sync() }()

	cleaner, err := cleanup.NewCleaner(cleanup.CleanerConfig{
		Log:      logger,
		URL:      viper.GetString("url"),
		Token:    viper.GetString("token"),
		Projects: viper.GetStringSlice("projects"),
		Groups:   viper.GetStringSlice("groups"),
		Delay:    viper.GetDuration("delay"),
	})
	if err != nil {
		return fmt.Errorf("failed to create cleaner: %w", err)
	}

	count, err := cleaner.Cleanup()
	if err != nil {
		return fmt.Errorf("failed to cleanup runners: %w", err)
	}

	logger.Infow(fmt.Sprintf("cleaned up %d runners", count), "count", count)

	return nil
}

func newLogger(logLevel string) (*zap.SugaredLogger, error) {
	logConfig := zap.NewProductionConfig()

	if logLevel == "" {
		logLevel = "info"
	}

	switch logLevel {
	case "debug":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	case "info":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
	case "warn":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.WarnLevel)
	case "error":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.ErrorLevel)
	case "panic":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.PanicLevel)
	default:
		return nil, errInvalidLogLevel
	}

	logger, err := logConfig.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to initialize logger: %w", err)
	}

	return logger.Sugar(), nil
}
