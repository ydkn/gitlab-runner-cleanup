# Build stage
FROM --platform=$BUILDPLATFORM golang:1.23 AS builder

ENV CGO_ENABLED=0
ENV GOPRIVATE=gitlab.com/ydkn/*

RUN mkdir -p /src
WORKDIR /src

COPY go.mod go.sum .git-credentials* /src/
RUN git config --global credential.helper "store --file=/src/.git-credentials"
RUN --mount=type=cache,target=/root/.cache/go-build \
  --mount=type=cache,target=/go/pkg \
  go mod download

COPY . /src
RUN mkdir -p dist

ARG TARGETOS TARGETARCH
ENV GOOS=$TARGETOS
ENV GOARCH=$TARGETARCH

RUN --mount=type=cache,target=/root/.cache/go-build \
  --mount=type=cache,target=/go/pkg \
  go build -ldflags="-w -s" -o dist/gitlab-runner-cleanup cmd/gitlab-runner-cleanup/*.go

# Run stage
FROM gcr.io/distroless/static:nonroot

COPY --from=builder /src/dist /usr/local/bin

CMD ["gitlab-runner-cleanup"]
